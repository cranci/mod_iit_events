<?php
/**
 * Helper class for IIT - Events
 *
 * @package    IIT.Events
 * @subpackage Modules
 * @link http://www.iit.it
 * @license        GNU/GPL, see LICENSE.php
 */

require_once(JPATH_BASE . '/components/com_events/api/ApiProvider.php');
require_once(JPATH_BASE . '/components/com_events/api/EventsListApiQuery.php');

class modIitEventsHelper
{
    /**
     * Retrieves the hello message
     *
     * @param array $params An object containing the module parameters
     * @access public
     */
    public static function getEvents($requestVars, $alias = null)
    {
        try {
            //unset($requestVars["count"]);
            unset($requestVars["type"]);
           
            $apiQuery = new EventsListApiQuery() ;
            //$apiQuery = self::setApiQuery($requestVars);
            $apiQuery->setFromDate(date('d-m-Y'));
            $eventsListApi =  ApiProvider::get('EventsList');
            $availableEvents = $eventsListApi->get($apiQuery);
        
            if(count($availableEvents) >= $requestVars["count"])
            {
                $result = array_slice($availableEvents,0,$requestVars["count"]);
            }
            else{
                $result = $availableEvents;
                
                $apiQuery = new EventsListApiQuery() ;
                $eventsListApi =  ApiProvider::get('EventsList');
                $allEvents = $eventsListApi->get($apiQuery);
                
                end($allEvents);         
                $key = key($allEvents);
                
                for($i = count($result); $i < $requestVars["count"]; $i++){
                    
                    array_unshift($result,$allEvents[$key-$i]);
                }
                
            }

        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
        
        function sortFunction( $a, $b ) {
        return strtotime($a->getEnd()) - strtotime($b->getEnd());
    }
    
        //usort($events, "sortFunction"); // sort by end date
        return array_reverse($result);
    }
    




    /*
public static function getEvents($newParams, $alias = null)
    {
        try {
            unset($newParams["count"]);
            unset($newParams["type"]);
           
            
            $apiQuery = new EventsListApiQuery($newParams);
            $apiQuery->setRelatedEvent($alias);
           
            
            $events = ApiProvider::get('EventsList')->get($apiQuery);
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
        
        
        function sortFunction( $a, $b ) {
        return strtotime($a->getEnd()) - strtotime($b->getEnd());
    }
    
        //usort($events, "sortFunction"); // sort by end date
        return $events;
    }
    */
    
    // :: FILTEREVENTS ::
    // take the oldest $limit events, if future events aren't enough
    // fills up the gaps with past events
    public static function filterEvents($evts, $limit) 
    {
        $actualEvents = array();
        $i = 0;
        $arrayPointer = 0;
        /*
        echo '<pre>';
        print_r($evts);
        echo '</pre>';
        */
        foreach ($evts as  $index => $e) //populate $futureEvents
        {
            
            if($i == $limit){break;}
            
            if(strtotime($e->getEnd()) >= strtotime(date('d-m-Y')))
            {
                if($arrayPointer == 0) {
                    $arrayPointer = $index-1;
                } 
                array_push($actualEvents, $e);
                $i++;
            }
            /*
             echo '<pre>';
        print_r($evts);
        echo '</pre>';
        die();*/
            
        } // END FOREACH
        
        
        if(count($i<$limit)){// there's space for past events...
            
            if($arrayPointer == 0) {// se non ci sono eventi futuri il puntatore è alla fine dell'array
                    $arrayPointer = count($evts)-1;
                } 
            
            for($i; $i<$limit; $i++){
                array_unshift($actualEvents, $evts[$arrayPointer]); //prepend past events
                $arrayPointer--;
            }
            
        }
        /*
        echo '<pre>';
        print_r($arrayPointer);
        echo '</pre>';
        echo '<pre>';
        print_r($evts);
        echo '</pre>';*/
        //die();
        return $actualEvents;
        
    }
    
}

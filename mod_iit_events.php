<?php
/**
 * Seltzlab IIT Events Module Entry Point
 *
 * @package    IIT.events
 * @subpackage Modules
 * @link http://www.iit.it
 * @license        GNU/GPL, see LICENSE.php
 */

// no direct access

defined('_JEXEC' ) or die;

$config = JFactory::getConfig();
$site =  explode('.', $config->get( 'sitename' ));

$count = $params->get('count');
$layout = $params->get('layout');

// CSS & layout for talk
if($site[0] == 'talk'){ 
    JHtml::stylesheet('media/mod_iit_events/css/talk.css');
    if($layout == 'list'){
        $layout .= '_talk';
    }
}
else
{
    JHtml::stylesheet('media/mod_iit_events/css/site.css');
}

$alias = JRequest::getVar('alias', null);

// Include the syndicate functions only once
require_once(dirname(__FILE__) . '/helper.php');
//$events = modIitEventsHelper::getEvents($params, $alias);



//$events = modIitEventsHelper::filterEvents(modIitEventsHelper::getEvents($params, $alias), $count);

$events = modIitEventsHelper::getEvents($params, $alias);

require(JModuleHelper::getLayoutPath('mod_iit_events', $layout ));
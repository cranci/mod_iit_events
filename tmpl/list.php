

<?php
// default - lista
// No direct access
defined('_JEXEC') or die;
$lang = JFactory::getLanguage();
//setlocale(LC_TIME, str_replace('-', '_', $lang->getTag()));

$item = JFactory::getApplication()->getMenu()->getItem( $params->get("menuvoice") );
$url = JRoute::_("index.php?option=com_events&Itemid=" . $item->id);

?>


<h2 class="iit-title"><?php echo $module->title;?> <a href="<?php echo $url;?>"><span class="fa fa-angle-right pull-right"></span></a></h2>
<div class="list-group iit-list-group event-list">
<?php if( count($events) > 0 ):?>
    <?php foreach($events as $ev): 
        
        if(empty($ev)){continue;}
        
        ?>
    <a href="<?php echo $ev->getDetailUrl($params->get('menuvoice'));?>" class="list-group-item iit-next-event">
      <div class="row">
        <div class="col-sm-1 col-xs-2 iit-next-event-calendar-container">
            <?php if($ev->getStart() != $ev->getEnd()) : ?>
            <div class="iit-next-event-calendar multiple">
                <span><?php echo $ev->getStart("%d", $lang->getTag()) . " " . substr($ev->getStart("%B", $lang->getTag()) , 0, 3 ); ?></span>
                <hr/>
                <span><?php echo $ev->getEnd("%d", $lang->getTag()) . " " . substr($ev->getEnd("%B", $lang->getTag()) , 0, 3 ); ?></span>
            </div>
            <?php else:?>
            <div class="iit-next-event-calendar">
                <span class="day"><?php echo $ev->getStart("%d", $lang->getTag());?></span>
                <span class="month"><?php echo substr($ev->getStart("%B", $lang->getTag()), 0, 3);?></span>
            </div>
            <?php endif;?>
        </div>
        <div class="col-sm-11 col-sm-10 col-xs-10">
            <h4 class="list-group-item-heading"><span class="iit-next-event-topic"><?php echo $ev->getCategory()->getNode($lang->getTag());?>.</span> <?php echo $ev->getName($lang->getTag());?></h4>
          <p class="list-group-item-text"><?php echo strip_tags($ev->getDescription($lang->getTag(), $params->get( 'charscount' )));?></p>
        </div>
      </div>
    </a>
    <?php endforeach;?>
<?php else:?>
    <div>No events found</div>
<?php endif;?>
</div>
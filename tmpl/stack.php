<?php
// default - lista
// No direct access
defined('_JEXEC') or die;
$lang = JFactory::getLanguage();
setlocale(LC_TIME, str_replace('-', '_', $lang->getTag()));
$nav_items = '';

$item = JFactory::getApplication()->getMenu()->getItem( $params->get("menuvoice") );
$url = JRoute::_("index.php?option=com_events&Itemid=" . $item->id);
?>
<h3 class="iit-title"><?php echo $module->title;?> <a href="<?php echo $url;?>"><span class="fa fa-angle-right pull-right"></span></a></h3>
    <?php
if( count($events) > 0 ):?>
    <div data-widgetkit="slideset" class="wk-slideset wk-slideset-default" id="slideset-random7" data-options='{"items_per_set":1,"navigation":<?php echo $params->get("navigation");?>,"index":<?php echo $params->get("startindex");?>,"title":0,"buttons":<?php echo $params->get("buttons");?>,"style":"default","width":"auto","height":"auto","effect":"<?php echo $params->get("effect");?>","autoplay":<?php echo $params->get("autoplay");?>,"interval":<?php echo $params->get("autoplayinterval");?>,"duration":<?php echo $params->get("effectduration");?>}'>
        <div>
            <div class="sets">
            <?php
                foreach($events as $ev):
                    $nav_items .= '<li class=""><span></span></li>';
            ?>
                <ul class="set">
                    <li>
                        <article class="wk-content">
                            <a href="<?php echo $ev->getDetailUrl($params->get('menuvoice'));?>" class="list-group-item iit-next-event">
                              <div>
                                <img src="<?php echo $ev->getLink(); ?>" class="thumbnail" />
                                <div>
                                    <h4 class="list-group-item-heading"><span class="iit-next-event-topic"><?php echo $ev->getCategory()->getAlias($lang->getTag());?></span> <?php echo $ev->getName($lang->getTag());?></h4>
                                    <p class="list-group-item-text"><?php echo $ev->getDescription($lang->getTag(), $params->get( 'charscount' ));?></p>
                                </div>
                              </div>
                            </a>
                        </article>
                    </li>
                </ul>
            <?php
                endforeach;
            ?>
            </div>
            <?php if($params->get("buttons") == 1):?>
                <div class="next"></div>
                <div class="prev"></div>
            <?php endif;?>
        </div>
        <?php if ($params->get("navigation") == 1):?>
            <ul class="nav icon">
                <?php echo $nav_items;?>
            </ul>
        <?php endif;?>
	</div>
<?php else:?>
    <div>No events found</div>
<?php endif;?>